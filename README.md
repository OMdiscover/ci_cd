# DESCRIPTION DU DEPOT
---
Ce dépôt est constitué de l'application vapormap qui  est une application web permettant de référencer des points GPS dans une base de données. Ces points peuvent être affichés sur une carte. Elle est écrite en python et utilise le framework Flask. L'objectif de ce dépôt est d'automatiser le déploiement de l'application vapormap de sorte qu'une intervention humaine ne soit plus requise après un push  sur le dépôt. Donc, en plus de l'application, ce dépôt est constitué de fichiers permettant de lancer une stack de conteneurs, créer le pipeline permettant d'automatiser les tests et le déploiement.

## Déploiement avec build des images en local
---
* Pour déployer l'application vapormap, il est tout d'abord nécessaire de clôner le projet sur votre machine en locale.
* Après avoir clôné le dépot, il faut donc ouvrir le fichier docker-compose.yaml et remplacer les adresses IP et ports des différents services par ceux de votre machine selon votre convenance.
* Ensuite, écrire les codes suivants pour pouvoir effectuer un build des Dockerfile:
* Pour l'api:
``` yaml
 build: 
         context: .
         dockerfile: Dockerfile_backend
```
* Pour le frontend:
```yaml
build: 
         context: .
         dockerfile: Dockerfile_frontend
```
Après ces étapes effactuées, il faut exécuter la commande suivante en étant dans le répertoire du projet :
```yaml
docker-compose up
```
## Déploiement en utilisant les images de la registry du projet
---
* Pour effectuer le déploiement, il faut lancer la commande docker-compose up. Mais avant de lancer cette commande, il faut ajuster dans le fichier docker-compose.yaml, les valeurs des variables d'environnement. 

* Dans les deux modes de déploiement, l' utilisation l'image MySQL stockée dans la registry du projet se fait sans problèmes.

```yaml
docker-compose up
```
## CI/CD
* Les jobs des stages de test et de build se lancent seulement s'il y'a modification sur l'un des fichiers à tester
---


