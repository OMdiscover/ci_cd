#!/bin/bash

export VAPOR_DBNAME

export VAPOR_DBUSER

export VAPOR_DBPASS

export VAPOR_DBHOST

flask db upgrade

exec gunicorn --bind 0.0.0.0:5000 wsgi:app
